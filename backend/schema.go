package backend

type AuthenticationResponse struct {
    Token string `json:"token"`
}
