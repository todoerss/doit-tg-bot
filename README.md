## DoIt telegram bot

### Build

To build the bot run
```bash
$ go build ./cmd/doit_bot
```

### Test

To run tests run
```bash
$ go test ./...
```


