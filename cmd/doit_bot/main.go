package main

import (
	"fmt"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/jonboulle/clockwork"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	doit "gitlab.com/todoerss/doit-tg-bot"
	"gitlab.com/todoerss/doit-tg-bot/backend"

	tele "gopkg.in/telebot.v3"
)

func formatTasks(tasks []*doit.Task) string {
    var str strings.Builder

    for i, task := range tasks {
        if task == nil {
            continue
        }

        deadline := task.Deadline.Format("2006/01/02")
        str.WriteString(fmt.Sprint(i+1, ". ", deadline, " ", task.Content))
        str.WriteRune('\n')
    }

    return str.String()
}

func getTaskStatus(doitBot *doit.Bot, chatID tele.ChatID) (string, error) {
    upcoming, err := doitBot.UpcomingTasks(chatID)
    if err != nil {
        return "", fmt.Errorf("get full task status: %v", err)
    }

    tomorrow, err := doitBot.TomorrowTasks(chatID)
    if err != nil {
        return "", fmt.Errorf("get full task status: %v", err)
    }

    status := fmt.Sprintf("Upcoming tasks:\n%s\nDue tomorrow:\n%s\n", formatTasks(upcoming), formatTasks(tomorrow))
    return status, nil
}

func main() {
    log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

    pref := tele.Settings{
        Token: os.Getenv("TOKEN"),
        Poller: &tele.LongPoller{Timeout: 10 * time.Second},
    } 

    var fakeClock clockwork.FakeClock
    clock := clockwork.NewRealClock()
    if os.Getenv("FAKE_CLOCK") == "1" {
        fakeClock = clockwork.NewFakeClock()
        fakeClock.Advance(time.Now().Sub(fakeClock.Now()))
        clock = fakeClock
    }

    log.Info().Time("now", clock.Now()).Bool("fake", fakeClock != nil).Msg("Clock initialized")

    var manager doit.SessionManager
    if os.Getenv("BACKEND_URL") != "" {
        raw := os.Getenv("BACKEND_URL")
        url, err := url.Parse(raw)
        if err != nil {
            log.Fatal().
                Str("backend_url", raw).
                Msg("Parse backend URL")
            os.Exit(1)
        }

        manager = backend.NewConnector(url)

        /*
        s, err := manager.NewUsingPassword("admin", "passpass123")
        if err != nil {
            log.Err(err).Msg("Login")
            return
        }

        exists, err := s.GroupExists("hello")
        if err != nil {
            log.Err(err).Msg("Group exists")
            return
        }

        //err = s.CreateGroup("bot test")
        if err != nil {
            log.Err(err).Msg("create Group")
            return
        }

        log.Info().Bool("Yes?", exists).Msg("inbox exists?")

        exists, err = s.ListExists("doit TODO", "Mobile UI")
        if err != nil {
            log.Err(err).Msg("Group exists")
            return
        }
        log.Info().Bool("Yes?????????", exists).Msg("list exists?")

        //err = s.CreateList("bot test", "foo")
        if err != nil {
            log.Err(err).Msg("create list")
            return
        }

        log.Info().Bool("Yes????", exists).Msg("list exists?")

        task, err := s.AddTaskUnderDeadline("bot test", "foo", "hello!", time.Now())
        log.Info().Msgf("error %v ||| task %#v", err, task)

        return
        */
    } else {
        manager = doit.NewSpySessionManager()
    }

    doitBot := doit.NewBot(manager, clock)

    b, err := tele.NewBot(pref)
    if err != nil {
        log.Err(err).Msg("Create bot")
        os.Exit(1)
        return
    }

    go doitBot.Poll(time.Date(0, 0, 0, 19, 0, 0, 0, time.UTC), func(chat tele.ChatID, tasks []*doit.Task, err error) {
        if err != nil {
            b.Send(chat, "Something went wrong when tried to notify you of tomorrow's tasks\n" + err.Error())
            return
        }

        if tasks == nil {
            return
        }

        b.Send(chat, "Tasks for tomorrow:\n" + formatTasks(tasks))
    })

    b.OnError = func(err error, c tele.Context) {
        log.Fatal().
            Err(err).
            Stack().
            Str("user_id", c.Sender().Username).
            Str("text", c.Message().Text).
            Msg("Panic")
    }

    b.SetCommands(
        tele.Command{Text: "login", Description: "<username> <password> Login to DoIt"},
        tele.Command{Text: "new", Description: "<date> <text> Add new task"},
    )

    b.Handle("/login", func(c tele.Context) error {
        log.Info().
            Int64("chat_id", c.Chat().ID).
            Str("user_id", c.Sender().Username).
            Str("text", c.Message().Text).
            Msg("Login request")

        args := c.Args()
        if len(args) != 2 {
            log.Info().
                Int64("chat_id", c.Chat().ID).
                Str("user_id", c.Sender().Username).
                Int("arguments_provided", len(args)).
                Msg("Login rejected since not enough arguments were provided")

            return c.Send("Please, provide your login and password separated with a space.")
        }

        err := doitBot.Login(tele.ChatID(c.Sender().ID), args[0], args[1])
        if err != nil {
            log.Err(err).
                Int64("chat_id", c.Chat().ID).
                Str("user_id", c.Sender().Username).
                Str("provided_doit_login", args[0]).
                Str("provided_doit_pass", args[1]).
                Msg("Failed to authenticate")

            return c.Send("Either login or password are probably incorrect.\nPlease try again.")
        }

        err = c.Send("Logged in successfully!")
        if err != nil {
            return err
        }

        status, err := getTaskStatus(doitBot, tele.ChatID(c.Chat().ID))
        if err != nil {
            log.Err(err).
                Int64("chat_id", c.Chat().ID).
                Str("user_id", c.Sender().Username).
                Str("provided_doit_login", args[0]).
                Str("provided_doit_pass", args[1]).
                Msg("Failed to retreive full task status")
            return c.Send(fmt.Sprintf("Something went wrong:\n%v", err))
        }

        return c.Send(status)
    })

    b.Handle("/show", func(c tele.Context) error {
        log.Info().
            Int64("chat_id", c.Chat().ID).
            Str("user_id", c.Sender().Username).
            Str("text", c.Message().Text).
            Msg("Show upcoming tasks")

        status, err := getTaskStatus(doitBot, tele.ChatID(c.Chat().ID))
        if err != nil {
            return c.Send(fmt.Sprintf("Something went wrong:\n%v", err))
        }

        return c.Send(status)
    })

    b.Handle("/new", func(c tele.Context) error {
        log.Info().
            Int64("chat_id", c.Chat().ID).
            Str("user_id", c.Sender().Username).
            Str("text", c.Message().Text).
            Msg("New task")

        upcoming, err := doitBot.HandleNewTask(c.Message())
        if err != nil {
            log.Err(err).
                Int64("chat_id", c.Chat().ID).
                Str("user_id", c.Sender().Username).
                Msg("New task")

            return c.Send(fmt.Sprintf("Something went wrong:\n%v", err))
        }

        tomorrow, err := doitBot.TomorrowTasks(tele.ChatID(c.Sender().ID))
        if err != nil {
            log.Err(err).
                Int64("chat_id", c.Chat().ID).
                Str("user_id", c.Sender().Username).
                Msg("Retreive tomorrow tasks after new task is created")

            return c.Send(fmt.Sprintf("Something went wrong:\n%v", err))
        }

        status := fmt.Sprintf("Upcoming tasks:\n%s\nDue tomorrow:\n%s\n", formatTasks(upcoming), formatTasks(tomorrow))
        return c.Send(status)
    })

    b.Handle("/advance", func(c tele.Context) error {
        if fakeClock == nil {
            return nil
        }

        offset, err := time.ParseDuration(c.Message().Payload)
        if err != nil {
            return c.Send("Couldn't parse the duration timestamp: " + err.Error() + "\n\nExamples: 1d, 13h43m")
        }

        fakeClock.Advance(offset)
        return c.Send("Current bot time: " + fakeClock.Now().Format("2006/01/02 15:04:05 -0700"))
    })

    log.Info().Msg("Bot initialized")

    b.Start()
}
