package doit_test

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/todoerss/doit-tg-bot"
	tele "gopkg.in/telebot.v3"

	"github.com/jonboulle/clockwork"
	"github.com/sanity-io/litter"
)


func TestBotCreatesInboxOnLogin(t *testing.T) {
    t.Run("telegram inbox exists", func(t *testing.T) {
        mgr := doit.NewSpySessionManager()
        mgr.Sessions["Isabelle"] = &doit.SpySession{
            Session: &doit.DummySession{},
            Groups: map[string]*doit.Group{
                "Inbox": {
                    ID: 0,
                    Name: "Inbox",
                    Lists: map[string]*doit.List{
                        "Telegram": {
                            ID: 0,
                            Name: "Telegram",
                            Tasks: nil,
                        },
                    },
                },
            },
        }

        bot := doit.NewBot(mgr, clockwork.NewRealClock())

        err := bot.Login(100, "Isabelle", "12345")
        if err != nil {
            t.Fatalf("got login error %q, want none", err)
        }

        if len(mgr.Sessions) != 1 {
            t.Fatalf("got %d opened sessions, wanted 1, sessions: %#v", len(mgr.Sessions), mgr.Sessions)
        }

        wantStat := doit.SpySessionStat{
            GroupsChecked: []string{"Inbox"},
            GroupsCreated: nil,
            ListsChecked: []doit.GroupListPair{{"Inbox", "Telegram"}},
            ListsCreated: nil,
        }

        doit.AssertSessionStat(t, mgr.Sessions["Isabelle"], wantStat)
    })

    t.Run("inbox group does not exist", func(t *testing.T) {
        mgr := doit.NewSpySessionManager()

        bot := doit.NewBot(mgr, clockwork.NewRealClock())

        err := bot.Login(100, "Isabelle", "12345")
        if err != nil {
            t.Fatalf("got login error %q, want none", err)
        }

        if len(mgr.Sessions) != 1 {
            t.Fatalf("got %d opened sessions, wanted 1, sessions: %#v", len(mgr.Sessions), mgr.Sessions)
        }

        wantStat := doit.SpySessionStat{
            GroupsChecked: []string{"Inbox"},
            GroupsCreated: []string{"Inbox"},
            ListsChecked: []doit.GroupListPair{{Group: "Inbox", List: "Telegram"}},
            ListsCreated: []doit.GroupListPair{{Group: "Inbox", List: "Telegram"}},
        }

        doit.AssertSessionStat(t, mgr.Sessions["Isabelle"], wantStat)
    })

    t.Run("telegram list does not exist", func(t *testing.T) {
        mgr := doit.NewSpySessionManager()
        mgr.Sessions["Isabelle"] = &doit.SpySession{
            Session: &doit.DummySession{},
            Groups: map[string]*doit.Group{
                "Inbox": {
                    ID: 0,
                    Name: "Inbox",
                    Lists: make(map[string]*doit.List),
                },
            },
        }

        bot := doit.NewBot(mgr, clockwork.NewRealClock())

        err := bot.Login(100, "Isabelle", "12345")
        if err != nil {
            t.Fatalf("got login error %q, want none", err)
        }

        if len(mgr.Sessions) != 1 {
            t.Fatalf("got %d opened sessions, wanted 1, sessions: %#v", len(mgr.Sessions), mgr.Sessions)
        }

        wantStat := doit.SpySessionStat{
            GroupsChecked: []string{"Inbox"},
            GroupsCreated: nil,
            ListsChecked: []doit.GroupListPair{{"Inbox", "Telegram"}},
            ListsCreated: []doit.GroupListPair{{"Inbox", "Telegram"}},
        }

        doit.AssertSessionStat(t, mgr.Sessions["Isabelle"], wantStat)
    })

    t.Run("two sessions where one has and the other doesn't have an inbox", func(t *testing.T) {
        mgr := doit.NewSpySessionManager()
        mgr.Sessions["Renren"] = &doit.SpySession{
            Session: &doit.DummySession{},
            Groups: map[string]*doit.Group{
                "Inbox": {
                    ID: 0,
                    Name: "Inbox",
                    Lists: map[string]*doit.List{
                        "Telegram": {
                            ID: 0,
                            Name: "Telegram",
                            Tasks: nil,
                        },
                    },
                },
            },
        }

        bot := doit.NewBot(mgr, clockwork.NewRealClock())

        err := bot.Login(100, "Isabelle", "12345")
        if err != nil {
            t.Fatalf("got login error %q, want none", err)
        }

        err = bot.Login(200, "Renren", "54321")
        if err != nil {
            t.Fatalf("got login error %q, want none", err)
        }

        if len(mgr.Sessions) != 2 {
            t.Fatalf("got %d opened sessions, wanted 2, sessions: %#v", len(mgr.Sessions), mgr.Sessions)
        }

        wantIsabelleStat := doit.SpySessionStat{
            GroupsChecked: []string{"Inbox"},
            GroupsCreated: []string{"Inbox"},
            ListsChecked: []doit.GroupListPair{{"Inbox", "Telegram"}},
            ListsCreated: []doit.GroupListPair{{"Inbox", "Telegram"}},
        }

        wantRenrenStat := doit.SpySessionStat{
            GroupsChecked: []string{"Inbox"},
            GroupsCreated: nil,
            ListsChecked: []doit.GroupListPair{{"Inbox", "Telegram"}},
            ListsCreated: nil,
        }

        doit.AssertSessionStat(t, mgr.Sessions["Isabelle"], wantIsabelleStat)
        doit.AssertSessionStat(t, mgr.Sessions["Renren"], wantRenrenStat)
    })
}

func TestBotOnNewTask(t *testing.T) {
    // The output task list is sorted by date and by alphabet within the same date,
    // So that the task indices are always the same, no matter the order in the backend response.
    aikoUser := &tele.User{
        ID: 100,
        IsBot: false,
    }

    userChat := &tele.Chat{
        ID: 0,
        Type: tele.ChatPrivate,
    }

    mgr := doit.NewSpySessionManager()
    b := doit.NewBot(mgr, clockwork.NewRealClock())

    t.Run("add task before login", func(t *testing.T) {
        taskMsg := &tele.Message{
            ID: 0,
            Sender: aikoUser,
            Unixtime: time.Now().Unix(),
            Chat: userChat,
            Text: "/task 2022/08/08 Foo",
            Payload: "2022/08/08 Foo",
        }

        _, err := b.HandleNewTask(taskMsg)
        if err == nil {
            t.Fatal("got no error, want some")
        }
    })

    err := b.Login(100, "Aiko", "69845")
    if err != nil {
        t.Fatalf("got login error %q, want none", err)
    }

    session := mgr.Sessions["Aiko"]

    t.Run("first task is added", func(t *testing.T) {
        taskMsg := &tele.Message{
            ID: 0,
            Sender: aikoUser,
            Unixtime: time.Now().Unix(),
            Chat: userChat,
            Text: "/task 2022/05/09 Walk outside",
            Payload: "2022/05/09 Walk outside",
        }

        gotTasks, err := b.HandleNewTask(taskMsg)
        if err != nil {
            t.Fatalf("got error '%s' when handling new task request, want none", err)
        }

        wantTasks := []*doit.Task{
            {
                ID: 0,
                Content: "Walk outside",
                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                Done: false,
            },
        }

        wantGroups := map[string]*doit.Group{
            "Inbox": {
                ID: 0,
                Name: "Inbox",
                Lists: map[string]*doit.List{
                    "Telegram": {
                        ID: 0,
                        Name: "Telegram",
                        Tasks: wantTasks,
                    },
                },
            },
        }

        if !reflect.DeepEqual(gotTasks, wantTasks) {
            t.Errorf("got bot-returned tasks\n%s\nwant\n%s", litter.Sdump(gotTasks), litter.Sdump(wantTasks))
        }

        if !reflect.DeepEqual(session.Groups, wantGroups) {
            t.Errorf("\n##### got groups %v\n##### want groups %v", litter.Sdump(session.Groups), litter.Sdump(wantGroups))
        }

    })

    t.Run("second task and goes above the previous one due to alphabetic order", func(t *testing.T) {
        taskMsg := &tele.Message{
            ID: 0,
            Sender: aikoUser,
            Unixtime: time.Now().Unix(),
            Chat: userChat,
            Text: "/task 2022/05/09 Get some food",
            Payload: "2022/05/09 Get some food",
        }

        gotTasks, err := b.HandleNewTask(taskMsg)
        if err != nil {
            t.Fatalf("got error '%s' when handling new task request, want none", err)
        }

        // --------
        // Even though the task was added later, it should be returned first, because of the alphabet order
        // --------
        wantTasks := []*doit.Task{
            {
                ID: 0,
                Content: "Get some food",
                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                Done: false,
            },
            {
                ID: 0,
                Content: "Walk outside",
                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                Done: false,
            },
        }

        wantGroups := map[string]*doit.Group{
            "Inbox": {
                ID: 0,
                Name: "Inbox",
                Lists: map[string]*doit.List{
                    "Telegram": {
                        ID: 0,
                        Name: "Telegram",
                        Tasks: []*doit.Task{
                            {
                                ID: 0,
                                Content: "Walk outside",
                                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "Get some food",
                                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                                Done: false,
                            },
                        },
                    },
                },
            },
        }

        if !reflect.DeepEqual(gotTasks, wantTasks) {
            t.Errorf("\n##### got bot-returned tasks %s\n##### want %s", litter.Sdump(gotTasks), litter.Sdump(wantTasks))
        }

        if !reflect.DeepEqual(session.Groups, wantGroups) {
            t.Errorf("\n##### got groups %v\n##### want groups %v", litter.Sdump(session.Groups), litter.Sdump(wantGroups))
        }

    })

    t.Run("third task that would go first due to alphabet, will go last due to the deadline", func(t *testing.T) {
        taskMsg := &tele.Message{
            ID: 0,
            Sender: aikoUser,
            Unixtime: time.Now().Unix(),
            Chat: userChat,
            Text: "/task 2022/05/10 Cast magic spells",
            Payload: "2022/05/10 Cast magic spells",
        }

        gotTasks, err := b.HandleNewTask(taskMsg)
        if err != nil {
            t.Fatalf("got error '%s' when handling new task request, want none", err)
        }

        // --------
        // The new task is lexicographically less than every previous task, but expires a day later, so it should go last.
        // --------
        wantTasks := []*doit.Task{
            {
                ID: 0,
                Content: "Get some food",
                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                Done: false,
            },
            {
                ID: 0,
                Content: "Walk outside",
                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                Done: false,
            },
            {
                ID: 0,
                Content: "Cast magic spells",
                Deadline: time.Date(2022, time.May, 10, 0, 0, 0, 0, time.UTC),
                Done: false,
            },
        }

        wantGroups := map[string]*doit.Group{
            "Inbox": {
                ID: 0,
                Name: "Inbox",
                Lists: map[string]*doit.List{
                    "Telegram": {
                        ID: 0,
                        Name: "Telegram",
                        Tasks: []*doit.Task{
                            {
                                ID: 0,
                                Content: "Walk outside",
                                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "Get some food",
                                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "Cast magic spells",
                                Deadline: time.Date(2022, time.May, 10, 0, 0, 0, 0, time.UTC),
                                Done: false,
                            },
                        },
                    },
                },
            },
        }

        if !reflect.DeepEqual(gotTasks, wantTasks) {
            t.Errorf("\n##### got bot-returned tasks %s\n##### want %s", litter.Sdump(gotTasks), litter.Sdump(wantTasks))
        }

        if !reflect.DeepEqual(session.Groups, wantGroups) {
            t.Errorf("\n##### got groups %v\n##### want groups %v", litter.Sdump(session.Groups), litter.Sdump(wantGroups))
        }
    })

    t.Run("bot returns only unfinished tasks", func(t *testing.T) {
        session.Groups["Inbox"].Lists["Telegram"].Tasks[0].Done = true

        taskMsg := &tele.Message{
            ID: 0,
            Sender: aikoUser,
            Unixtime: time.Now().Unix(),
            Chat: userChat,
            Text: "/task 2022/05/08 Purr like a cat",
            Payload: "2022/05/08 Purr like a cat",
        }

        gotTasks, err := b.HandleNewTask(taskMsg)
        if err != nil {
            t.Fatalf("got error '%s' when handling new task request, want none", err)
        }

        wantTasks := []*doit.Task{
            {
                ID: 0,
                Content: "Purr like a cat",
                Deadline: time.Date(2022, time.May, 8, 0, 0, 0, 0, time.UTC),
                Done: false,
            },
            {
                ID: 0,
                Content: "Get some food",
                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                Done: false,
            },
            {
                ID: 0,
                Content: "Cast magic spells",
                Deadline: time.Date(2022, time.May, 10, 0, 0, 0, 0, time.UTC),
                Done: false,
            },
        }

        wantGroups := map[string]*doit.Group{
            "Inbox": {
                ID: 0,
                Name: "Inbox",
                Lists: map[string]*doit.List{
                    "Telegram": {
                        ID: 0,
                        Name: "Telegram",
                        Tasks: []*doit.Task{
                            {
                                ID: 0,
                                Content: "Walk outside",
                                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                                Done: true,
                            },
                            {
                                ID: 0,
                                Content: "Get some food",
                                Deadline: time.Date(2022, time.May, 9, 0, 0, 0, 0, time.UTC),
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "Cast magic spells",
                                Deadline: time.Date(2022, time.May, 10, 0, 0, 0, 0, time.UTC),
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "Purr like a cat",
                                Deadline: time.Date(2022, time.May, 8, 0, 0, 0, 0, time.UTC),
                                Done: false,
                            },
                        },
                    },
                },
            },
        }

        if !reflect.DeepEqual(gotTasks, wantTasks) {
            t.Errorf("\n##### got bot-returned tasks %s\n##### want %s", litter.Sdump(gotTasks), litter.Sdump(wantTasks))
        }

        if !reflect.DeepEqual(session.Groups, wantGroups) {
            t.Errorf("\n##### got groups %v\n##### want groups %v", litter.Sdump(session.Groups), litter.Sdump(wantGroups))
        }
    })

    t.Run("user can add tasks in the past", func(t *testing.T) {
        clock := clockwork.NewFakeClock()
        clock.Advance(time.Now().Sub(clock.Now()))

        today := clock.Now()
        today = time.Date(today.Year(), today.Month(), today.Day(), 0, 0, 0, 0, time.UTC)
        todayStr := today.Format("2006/01/02")

        mgr := doit.NewSpySessionManager()
        b := doit.NewBot(mgr, clockwork.NewRealClock())

        err := b.Login(100, "Aiko", "69845")
        if err != nil {
            t.Fatalf("got login error %q, want none", err)
        }

        taskMsg := &tele.Message{
            ID: 0,
            Sender: aikoUser,
            Unixtime: time.Now().Unix(),
            Chat: userChat,
            Text: "/task " + todayStr + " Foo",
            Payload: todayStr + " Foo",
        }

        gotTasks, err := b.HandleNewTask(taskMsg)
        if err != nil {
            t.Fatalf("got error '%s' when handling new task request, want none", err)
        }

        wantTasks := []*doit.Task{
            {
                ID: 0,
                Content: "Foo",
                Deadline: today,
                Done: false,
            },
        }

        doit.AssertBotReturnedTasks(t, gotTasks, wantTasks)
    })
}

func TestBotAddTaskTwoUsers(t *testing.T) {
    aikoUser := &tele.User{
        ID: 100,
        IsBot: false,
    }

    rikakoUser := &tele.User{
        ID: 200,
        IsBot: false,
    }

    userChat := &tele.Chat{
        ID: 0,
        Type: tele.ChatPrivate,
    }

    mgr := doit.NewSpySessionManager()

    b := doit.NewBot(mgr, clockwork.NewRealClock())

    err := b.Login(100, "Aiko", "69845")
    if err != nil {
        t.Fatalf("got login error %q, want none", err)
    }

    err = b.Login(200, "Rikako", "69845")
    if err != nil {
        t.Fatalf("got login error %q, want none", err)
    }

    aikoTaskMsg := &tele.Message{
        ID: 0,
        Sender: aikoUser,
        Unixtime: time.Now().Unix(),
        Chat: userChat,
        Text: "/task 2022/02/02 Fly on a train",
        Payload: "2022/02/02 Fly on a train",
    }

    gotAikoTasks, err := b.HandleNewTask(aikoTaskMsg)
    if err != nil {
        t.Fatalf("got error '%s' when handling new task request, want none", err)
    }

    rikakoTaskMsg := &tele.Message{
        ID: 0,
        Sender: rikakoUser,
        Unixtime: time.Now().Unix(),
        Chat: userChat,
        Text: "/task 2022/02/02 Bark from happiness",
        Payload: "2022/02/02 Bark from happiness",
    }

    gotRikakoTasks, err := b.HandleNewTask(rikakoTaskMsg)
    if err != nil {
        t.Fatalf("got error '%s' when handling new task request, want none", err)
    }

    wantAikoTasks := []*doit.Task{
        {
            ID: 0,
            Content: "Fly on a train",
            Deadline: time.Date(2022, time.February, 2, 0, 0, 0, 0, time.UTC),
            Done: false,
        },
    }

    wantRikakoTasks := []*doit.Task{
        {
            ID: 0,
            Content: "Bark from happiness",
            Deadline: time.Date(2022, time.February, 2, 0, 0, 0, 0, time.UTC),
            Done: false,
        },
    }

    wantAikoGroups := map[string]*doit.Group{
        "Inbox": {
            ID: 0,
            Name: "Inbox",
            Lists: map[string]*doit.List{
                "Telegram": {
                    ID: 0,
                    Name: "Telegram",
                    Tasks: wantAikoTasks,
                },
            },
        },
    }

    wantRikakoGroups := map[string]*doit.Group{
        "Inbox": {
            ID: 0,
            Name: "Inbox",
            Lists: map[string]*doit.List{
                "Telegram": {
                    ID: 0,
                    Name: "Telegram",
                    Tasks: wantRikakoTasks,
                },
            },
        },
    }

    doit.AssertBotReturnedTasks(t, gotAikoTasks, wantAikoTasks)
    doit.AssertGroups(t, mgr.Sessions["Aiko"].Groups, wantAikoGroups)

    doit.AssertBotReturnedTasks(t, gotRikakoTasks, wantRikakoTasks)
    doit.AssertGroups(t, mgr.Sessions["Rikako"].Groups, wantRikakoGroups)
}

func TestBotUpcomingTasks(t *testing.T) {
    clock := clockwork.NewFakeClockAt(time.Now())

    now := clock.Now()
    today := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC)
    tomorrow := now.AddDate(0, 0, 1)
    yesterday := now.AddDate(0, 0, -1)

    mgr := doit.NewSpySessionManager()
    mgr.Sessions["Aiko"] = &doit.SpySession{
        Session: &doit.DummySession{},
        Groups: map[string]*doit.Group{
            "Inbox": {
                ID: 0,
                Name: "Inbox",
                Lists: map[string]*doit.List{
                    "Telegram": {
                        ID: 0,
                        Name: "Telegram",
                        Tasks: []*doit.Task{
                            {
                                ID: 0,
                                Content: "Y",
                                Deadline: yesterday,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "Y done",
                                Deadline: yesterday,
                                Done: true,
                            },
                            {
                                ID: 0,
                                Content: "D",
                                Deadline: today,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "D done",
                                Deadline: today,
                                Done: true,
                            },
                            {
                                ID: 0,
                                Content: "T",
                                Deadline: tomorrow,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "T done",
                                Deadline: tomorrow,
                                Done: true,
                            },
                        },
                    },
                },
            },
        },
    }

    b := doit.NewBot(mgr, clock)

    err := b.Login(100, "Aiko", "69845")
    if err != nil {
        t.Fatalf("got login error %q, want none", err)
    }

    wantTasks := []*doit.Task{
        {
            ID: 0,
            Content: "D",
            Deadline: today,
            Done: false,
        },
        {
            ID: 0,
            Content: "T",
            Deadline: tomorrow,
            Done: false,
        },
    }

    gotTasks, err := b.UpcomingTasks(100)
    if err != nil {
        t.Fatalf("get upcoming tasks: got error '%v', want none", err)
    }

    doit.AssertBotReturnedTasks(t, gotTasks, wantTasks)
}

func TestBotTommorowTasks(t *testing.T) {
    clock := clockwork.NewFakeClock()

    clock.Advance(time.Now().Sub(clock.Now()))

    tomorrow := clock.Now().AddDate(0, 0, 1)
    tomorrow = time.Date(tomorrow.Year(), tomorrow.Month(), tomorrow.Day(), 0, 0, 0, 0, time.UTC)

    afterTomorrow := tomorrow.AddDate(0, 0, 1)
    today := tomorrow.AddDate(0, 0, -1)

    mgr := doit.NewSpySessionManager()
    mgr.Sessions["Aiko"] = &doit.SpySession{
        Session: &doit.DummySession{},
        Groups: map[string]*doit.Group{
            "Inbox": {
                ID: 0,
                Name: "Inbox",
                Lists: map[string]*doit.List{
                    "Telegram": {
                        ID: 0,
                        Name: "Telegram",
                        Tasks: []*doit.Task{
                            {
                                ID: 0,
                                Content: "A",
                                Deadline: tomorrow,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "B",
                                Deadline: afterTomorrow,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "C",
                                Deadline: tomorrow,
                                Done: true,
                            },
                            {
                                ID: 0,
                                Content: "D",
                                Deadline: tomorrow,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "E",
                                Deadline: today,
                                Done: false,
                            },
                        },
                    },
                },
            },
        },
    }

    b := doit.NewBot(mgr, clock)

    err := b.Login(100, "Aiko", "69845")
    if err != nil {
        t.Fatalf("got login error %q, want none", err)
    }

    wantTasks := []*doit.Task{
        nil,
        {
            ID: 0,
            Content: "A",
            Deadline: tomorrow,
            Done: false,
        },
        {
            ID: 0,
            Content: "D",
            Deadline: tomorrow,
            Done: false,
        },
        nil,
    }

    gotTasks, err := b.TomorrowTasks(100)
    if err != nil {
        t.Fatalf("get tomorrow tasks: got error '%v', want none", err)
    }

    doit.AssertBotReturnedTasks(t, gotTasks, wantTasks)
}

func TestBotPoll(t *testing.T) {
    clock := clockwork.NewFakeClockAt(time.Date(2022, time.May, 8, 19, 0, 0, 0, time.UTC))
    clock.Advance(time.Now().Sub(clock.Now()))

    today := clock.Now()
    today = time.Date(today.Year(), today.Month(), today.Day(), 0, 0, 0, 0, time.UTC)

    tomorrow := today.AddDate(0, 0, 1)
    afterTomorrow := today.AddDate(0, 0, 2)
    afterAfterTomorrow := today.AddDate(0, 0, 3)

    mgr := doit.NewSpySessionManager()
    mgr.Sessions["Aiko"] = &doit.SpySession{
        Session: &doit.DummySession{},
        Groups: map[string]*doit.Group{
            "Inbox": {
                ID: 0,
                Name: "Inbox",
                Lists: map[string]*doit.List{
                    "Telegram": {
                        ID: 0,
                        Name: "Telegram",
                        Tasks: []*doit.Task{
                            {
                                ID: 0,
                                Content: "Today 1",
                                Deadline: today,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "Today 2",
                                Deadline: today,
                                Done: true,
                            },
                            {
                                ID: 0,
                                Content: "Tomorrow 1",
                                Deadline: tomorrow,
                                Done: true,
                            },
                            {
                                ID: 0,
                                Content: "Tomorrow 2",
                                Deadline: tomorrow,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "After tomorrow 1",
                                Deadline: afterTomorrow,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "After tomorrow 2",
                                Deadline: afterTomorrow,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "After after tomorrow 1",
                                Deadline: afterAfterTomorrow,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "After after tomorrow 2",
                                Deadline: afterAfterTomorrow,
                                Done: true,
                            },
                        },
                    },
                },
            },
        },
    }

    mgr.Sessions["Rikako"] = &doit.SpySession{
        Session: &doit.DummySession{},
        Groups: map[string]*doit.Group{
            "Inbox": {
                ID: 0,
                Name: "Inbox",
                Lists: map[string]*doit.List{
                    "Telegram": {
                        ID: 0,
                        Name: "Telegram",
                        Tasks: []*doit.Task{
                            {
                                ID: 0,
                                Content: "Today",
                                Deadline: today,
                                Done: true,
                            },
                            {
                                ID: 0,
                                Content: "Tomorrow",
                                Deadline: tomorrow,
                                Done: false,
                            },
                            {
                                ID: 0,
                                Content: "After tomorrow",
                                Deadline: afterTomorrow,
                                Done: true,
                            },
                            {
                                ID: 0,
                                Content: "After after tomorrow",
                                Deadline: afterAfterTomorrow,
                                Done: false,
                            },
                        },
                    },
                },
            },
        },
    }

    bot := doit.NewBot(mgr, clock)

    err := bot.Login(100, "Aiko", "12345")
    if err != nil {
        t.Fatalf("got login error %q, want none", err)
    }

    err = bot.Login(200, "Rikako", "12345")
    if err != nil {
        t.Fatalf("got login error %q, want none", err)
    }

    callCount := 0
    wantTasks := map[tele.ChatID][]*doit.Task{
        100: {
            nil,
            {
                ID: 0,
                Content: "After tomorrow 1",
                Deadline: afterTomorrow,
                Done: false,
            },
            {
                ID: 0,
                Content: "After tomorrow 2",
                Deadline: afterTomorrow,
                Done: false,
            },
            nil,
        },
        200: {
            nil,
            nil,
        },
    }

    pollCallback := func(chatID tele.ChatID, gotTasks []*doit.Task, err error) {
        callCount += 1
        if err != nil {
            t.Errorf("got error '%v' in poll callback for user %q, want none", err, chatID)
            return
        }

        t.Logf("↓ for user %q", chatID)
        doit.AssertBotReturnedTasks(t, gotTasks, wantTasks[chatID])
    }

    go bot.Poll(time.Date(0, 0, 0, 19, 0, 0, 0, time.UTC), pollCallback)

    clock.BlockUntil(1)

    if callCount != 0 {
        t.Fatalf("when started polling, got %d callback calls, want 0", callCount)
    }

    clock.Advance(24 * time.Hour)
    clock.BlockUntil(1)

    if callCount != 2 {
        t.Fatalf("after one day, got %d callback calls, want 2", callCount)
    }

    wantTasks = map[tele.ChatID][]*doit.Task{
        100: {
            nil,
            nil,
            {
                ID: 0,
                Content: "After after tomorrow 1",
                Deadline: afterAfterTomorrow,
                Done: false,
            },
        },
        200: {
            {
                ID: 0,
                Content: "After after tomorrow",
                Deadline: afterAfterTomorrow,
                Done: false,
            },
        },
    }

    clock.Advance(24 * time.Hour)
    clock.BlockUntil(1)

    if callCount != 4 {
        t.Fatalf("after two days, got %d callback calls, want 4", callCount)
    }

    wantTasks = map[tele.ChatID][]*doit.Task{
        100: {
            nil,
        },
        200: {
            nil,
        },
    }

    clock.Advance(24 * time.Hour)
    clock.BlockUntil(1)

    if callCount != 6 {
        t.Fatalf("after three days, got %d callback calls, want 6", callCount)
    }

    wantTasks = map[tele.ChatID][]*doit.Task{
        100: {},
        200: {},
    }

    clock.Advance(24 * time.Hour)
    clock.BlockUntil(1)

    if callCount != 8 {
        t.Fatalf("after four days, got %d callback calls, want 8", callCount)
    }
}
