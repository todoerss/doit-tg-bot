package doit

type SessionManager interface {
    NewUsingPassword(username, pass string) (Session, error)
}
