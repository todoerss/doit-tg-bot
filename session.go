package doit

import (
    "time"
)

type TaskID int64
type ListID int64
type GroupID int64

type Task struct {
    ID TaskID `json:"id"`
    ListID ListID `json:"list"`
    Content string `json:"name"`
    Deadline time.Time `json:"deadline"`
    Done bool `json:"is_done"`
}

func (t *Task) Update(s Session, mod func(*Task)) error {
    mod(t)
    s.UpdateTask(t)
    return nil
}

type Group struct {
    ID GroupID `json:"id"`
    Name string `json:"name"`
    Lists map[string]*List
}

type List struct {
    ID ListID `json:"id"`
    GroupID GroupID `json:"group"`
    Name string `json:"name"`
    Tasks []*Task
}

type Session interface {
    GroupExists(name string) (bool, error)
    CreateGroup(name string) error

    ListExists(group, name string) (bool, error)
    CreateList(group, name string) error

    GetList(group, list string) (*List, error)

    AddTask(group, list, content string) (*Task, error)
    AddTaskUnderDeadline(group, list, content string, until time.Time) (*Task, error)
    UpdateTask(*Task) error
    RemoveTask(group, list, id string) error
}
