module gitlab.com/todoerss/doit-tg-bot

go 1.17

require (
	github.com/jonboulle/clockwork v0.3.0
	github.com/rs/zerolog v1.26.1
	github.com/sanity-io/litter v1.5.5
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d
	gopkg.in/telebot.v3 v3.0.0
)
