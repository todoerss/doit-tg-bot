package doit

import (
    "fmt"
    "reflect"
    "testing"
    "time"

    "github.com/sanity-io/litter"
)

type SpySessionManager struct {
    Sessions map[string]*SpySession
}

func NewSpySessionManager() *SpySessionManager {
    return &SpySessionManager{
        Sessions: make(map[string]*SpySession),
    }
}

func (m *SpySessionManager) NewUsingPassword(username, pass string) (Session, error) {
    session, exists := m.Sessions[username]
    if exists {
        return session, nil
    }

    session = &SpySession{
        Session: &DummySession{},
        Groups: make(map[string]*Group),
    }
    m.Sessions[username] = session

    return session, nil
}

type GroupListPair struct {
    Group, List string
}

type SpySessionStat struct {
    GroupsChecked []string
    GroupsCreated []string
    ListsChecked []GroupListPair
    ListsCreated []GroupListPair
}

type SpySession struct {
    Groups map[string]*Group
    
    Stat SpySessionStat
    Session
}

func (s *SpySession) GroupExists(name string) (bool, error) {
    s.Stat.GroupsChecked = append(s.Stat.GroupsChecked, name)

    _, exists := s.Groups[name]
    return exists, nil
}

func (s *SpySession) CreateGroup(name string) error {
    s.Stat.GroupsCreated = append(s.Stat.GroupsCreated, name)

    s.Groups[name] = &Group{
        ID: 0,
        Name: name,
        Lists: make(map[string]*List),
    }

    return nil
}

func (s *SpySession) ListExists(group, name string) (bool, error) {
    s.Stat.ListsChecked = append(s.Stat.ListsChecked, GroupListPair{group, name})

    g, exists := s.Groups[group]
    if !exists {
        return false, fmt.Errorf("check list %q in group %q: group doesn't exist", name, group)
    }

    _, exists = g.Lists[name]

    return exists, nil
}

func (s *SpySession) CreateList(group, name string) error {
    s.Stat.ListsCreated = append(s.Stat.ListsCreated, GroupListPair{group, name})

    g, exists := s.Groups[group]
    if !exists {
        return fmt.Errorf("create list %q in group %q: group doesn't exist", name, group)
    }

    g.Lists[name] = &List{
        ID: 0,
        Name: name,
        Tasks: nil,
    }

    return nil
}

func (s *SpySession) GetList(group, name string) (*List, error) {
    s.Stat.ListsChecked = append(s.Stat.ListsChecked, GroupListPair{group, name})

    g, exists := s.Groups[group]
    if !exists {
        return nil, fmt.Errorf("group %q doesn't exist", group)
    }

    l, exists := g.Lists[name]
    if !exists {
        return nil, fmt.Errorf("list %q doesn't exist", name)
    }

    listCopy := &List{
        ID: l.ID,
        Name: l.Name,
        Tasks: nil,
    }

    for _, task := range l.Tasks {
        listCopy.Tasks = append(listCopy.Tasks, &Task{
            ID: task.ID,
            Content: task.Content,
            Deadline: task.Deadline,
            Done: task.Done,
        })
    }

    return listCopy, nil
}

func (s *SpySession) AddTaskUnderDeadline(group, list, description string, until time.Time) (*Task, error) {
    g, exists := s.Groups[group]
    if !exists {
        return nil, fmt.Errorf("add task under deadline: in group %q: group doesn't exist", group)
    }

    l, exists := g.Lists[list]
    if !exists {
        return nil, fmt.Errorf("add task under deadline: in group %q: in list %q: list doesn't exist", group, list)
    }

    task := &Task{
        ID: 0,
        Content: description,
        Deadline: until,
        Done: false,
    }

    l.Tasks = append(l.Tasks, task)

    return task, nil
}


type DummySession struct {}

func (s *DummySession) GroupExists(name string) (bool, error) { return false, nil }
func (s *DummySession) CreateGroup(name string) error { return nil }
func (s *DummySession) ListExists(group, name string) (bool, error) { return false, nil }
func (s *DummySession) CreateList(group, name string) error { return nil }
func (s *DummySession) GetList(group, list string) (*List, error) { return nil, nil }
func (s *DummySession) AddTask(group, list, content string) (*Task, error) { return nil, nil }
func (s *DummySession) AddTaskUnderDeadline(group, list, content string, until time.Time) (*Task, error) { return nil, nil }
func (s *DummySession) UpdateTask(*Task) error { return nil }
func (s *DummySession) RemoveTask(group, list, id string) error { return nil }

// ----------------------------------------------------------------------------------------------------------

func AssertBotReturnedTasks(t *testing.T, got, want []*Task) {
    t.Helper()

    if !reflect.DeepEqual(got, want) {
        t.Errorf("\n##### got bot-returned tasks %s\n##### want %s", litter.Sdump(got), litter.Sdump(want))
    }
}

func AssertGroups(t *testing.T, got, want map[string]*Group) {
    t.Helper()

    if !reflect.DeepEqual(got, want) {
        t.Errorf("\n##### got groups %v\n##### want groups %v", litter.Sdump(got), litter.Sdump(want))
    }
}

func AssertSessionStat(t *testing.T, session *SpySession, want SpySessionStat) {
    t.Helper()

    if !reflect.DeepEqual(session.Stat.GroupsChecked, want.GroupsChecked) {
        t.Errorf("got %v group check calls, want %v", session.Stat.GroupsChecked, want.GroupsChecked)
    }

    if !reflect.DeepEqual(session.Stat.GroupsCreated, want.GroupsCreated) {
        t.Errorf("got %v group creation calls, want %v", session.Stat.GroupsCreated, want.GroupsCreated)
    }

    if !reflect.DeepEqual(session.Stat.ListsChecked, want.ListsChecked) {
        t.Errorf("got %v list check calls, want %v", session.Stat.ListsChecked, want.ListsChecked)
    }

    if !reflect.DeepEqual(session.Stat.ListsCreated, want.ListsCreated) {
        t.Errorf("got %v list creation calls, want %v", session.Stat.ListsCreated, want.ListsCreated)
    }
}
