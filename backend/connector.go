package backend

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"gitlab.com/todoerss/doit-tg-bot"
)

type Connector struct {
    backendURL *url.URL
}

func NewConnector(url *url.URL) *Connector {
    return &Connector{
        backendURL: url,
    }
}

func getEndpoint(url *url.URL, epPath string) string {
    return url.Scheme + "://" + path.Join(url.Host, epPath) + "/"
}

func (c *Connector) NewUsingPassword(user, pass string) (doit.Session, error) {
    reqBodyRaw := fmt.Sprintf("{\n\t\"username\":\"%s\",\n\t\"password\":\"%s\"\n}", user, pass)
    reqBody := strings.NewReader(reqBodyRaw)

    ep := getEndpoint(c.backendURL, "/login")

    log.Debug().Str("endpoint", ep).Str("body", reqBodyRaw).Msg("Logging in...")

    req, err := http.NewRequest("POST", ep, reqBody)
    if err != nil {
        return nil, fmt.Errorf("initialize request: %v", err)
    }

    req.Header.Add("Content-Type", "application/json")

    res, err := http.DefaultClient.Do(req)
    if err != nil {
        return nil, fmt.Errorf("request login: %v", err)
    }
    defer res.Body.Close()

    resBody, err := io.ReadAll(res.Body)
    if err != nil {
        return nil, fmt.Errorf("read resonse: %v", err)
    }

    if res.StatusCode != http.StatusOK {
        return nil, fmt.Errorf("request login: %s (%s)", res.Status, resBody)
    }

    var auth AuthenticationResponse
    err = json.Unmarshal(resBody, &auth)
    if err != nil {
        return nil, fmt.Errorf("unmarshal response: %v", err)
    }

    return &Session{
        backendURL: c.backendURL,
        token: auth.Token,
    }, nil
}

type Session struct {
    backendURL *url.URL
    token string
}

func (s *Session) newRequest(method string, apiPath string, body io.Reader) (*http.Request, error) {
    ep := getEndpoint(s.backendURL, apiPath)

    req, err := http.NewRequest(method, ep, body)
    if err != nil {
        return nil, fmt.Errorf("initialize request: %v", err)
    }

    req.Header.Add("Authorization", "Token " + s.token)
    req.Header.Add("Content-Type", "application/json")
    return req, nil
}

func (s *Session) getGroups() ([]*doit.Group, error) {
    req, err := s.newRequest("GET", "/todo/groups", nil)
    if err != nil {
        return nil, fmt.Errorf("form request: %v", err)
    }
    
    res, err := http.DefaultClient.Do(req)
    if err != nil {
        return nil, fmt.Errorf("retreive groups: %v", err)
    }
    defer res.Body.Close()

    resBody, err := io.ReadAll(res.Body)
    if err != nil {
        return nil, fmt.Errorf("read resonse: %v", err)
    }

    if res.StatusCode >= 300 {
        return nil, fmt.Errorf("retreive groups: %v (%s)", res.Status, resBody)
    }

    var groups []*doit.Group
    err = json.Unmarshal(resBody, &groups)
    if err != nil {
        return nil, fmt.Errorf("unmarshal response: %v", err)
    }

    return groups, nil

}

func (s *Session) GroupExists(name string) (bool, error) {
    groups, err := s.getGroups()
    if err != nil {
        return false, err
    }

    for i := range groups {
        if groups[i].Name == name {
            return true, nil
        }
    }

    return false, nil
}

func (s *Session) createGroup(name string) (*doit.Group, error) {
    reqBody := strings.NewReader(fmt.Sprintf(`{"name":"%s"}`, name))

    req, err := s.newRequest("POST", "/todo/groups", reqBody)
    if err != nil {
        return nil, fmt.Errorf("form request: %v", err)
    }
    
    res, err := http.DefaultClient.Do(req)
    if err != nil {
        return nil, fmt.Errorf("retreive groups: %v", err)
    }
    defer res.Body.Close()

    resBody, err := io.ReadAll(res.Body)
    if err != nil {
        return nil, fmt.Errorf("read resonse: %v", err)
    }

    if res.StatusCode >= 300 {
        return nil, fmt.Errorf("retreive groups: %v (%s)", res.Status, resBody)
    }

    var group *doit.Group
    err = json.Unmarshal(resBody, &group)
    if err != nil {
        return nil, fmt.Errorf("unmarshal response: %v", err)
    }

    return group, nil
}

func (s *Session) CreateGroup(name string) error {
    _, err := s.createGroup(name)
    return err
}

func (s *Session) getLists() ([]*doit.List, error) {
    req, err := s.newRequest("GET", "/todo/lists", nil)
    if err != nil {
        return nil, fmt.Errorf("form request: %v", err)
    }
    
    res, err := http.DefaultClient.Do(req)
    if err != nil {
        return nil, fmt.Errorf("retreive lists: %v", err)
    }
    defer res.Body.Close()

    resBody, err := io.ReadAll(res.Body)
    if err != nil {
        return nil, fmt.Errorf("read resonse: %v", err)
    }

    if res.StatusCode >= 300 {
        return nil, fmt.Errorf("retreive groups: %v (%s)", res.Status, resBody)
    }

    var lists []*doit.List
    err = json.Unmarshal(resBody, &lists)
    if err != nil {
        return nil, fmt.Errorf("unmarshal response: %v", err)
    }

    return lists, nil
}

func (s *Session) getTasks() ([]*doit.Task, error) {
    req, err := s.newRequest("GET", "/todo/entries", nil)
    if err != nil {
        return nil, fmt.Errorf("form request: %v", err)
    }
    
    res, err := http.DefaultClient.Do(req)
    if err != nil {
        return nil, fmt.Errorf("retreive tasks: %v", err)
    }
    defer res.Body.Close()

    resBody, err := io.ReadAll(res.Body)
    if err != nil {
        return nil, fmt.Errorf("read resonse: %v", err)
    }

    if res.StatusCode >= 300 {
        return nil, fmt.Errorf("retreive tasks: %v (%s)", res.Status, resBody)
    }

    var tasks []*doit.Task
    err = json.Unmarshal(resBody, &tasks)
    if err != nil {
        return nil, fmt.Errorf("unmarshal response: %v", err)
    }

    return tasks, nil
}

func (s *Session) getGroup(name string) (*doit.Group, error) {
    groups, err := s.getGroups()
    if err != nil {
        return nil, fmt.Errorf("get groups: %v", err)
    }

    var group *doit.Group
    for i := range groups {
        if groups[i].Name == name {
            group = groups[i]
        }
    }

    if group == nil {
        return nil, fmt.Errorf("find group %q: not found", name)
    }

    return group, nil
}

func (s *Session) GetList(groupName, listName string) (*doit.List, error) {
    group, err := s.getGroup(groupName)
    if err != nil {
        return nil, fmt.Errorf("get group %q: %v", groupName, err)
    }

    lists, err := s.getLists()
    if err != nil {
        return nil, fmt.Errorf("get lists: %v", err)
    }

    var list *doit.List
    for i := range lists {
        if lists[i].GroupID == group.ID {
            list = lists[i]
            break
        }
    }

    if list == nil {
        return nil, nil
    }

    tasks, err := s.getTasks()
    if err != nil {
        return nil, fmt.Errorf("get tasks: %v", err)
    }

    for _, task := range tasks {
        if task.ListID != list.ID {
            continue
        }

        list.Tasks = append(list.Tasks, task)
    }

    return list, nil
}

func (s *Session) ListExists(group, name string) (bool, error) {
    list, err := s.GetList(group, name)
    if err != nil {
        return false, err
    }

    return list != nil, nil
}

func (s *Session) CreateList(groupName, listName string) error {
    group, err := s.getGroup(groupName)
    if err != nil {
        return fmt.Errorf("get group: %v", err)
    }

    reqBody := strings.NewReader(fmt.Sprintf(`{"group": %d,"name":"%s"}`, group.ID, listName))

    req, err := s.newRequest("POST", "/todo/lists", reqBody)
    if err != nil {
        return fmt.Errorf("form request: %v", err)
    }
    
    res, err := http.DefaultClient.Do(req)
    if err != nil {
        return fmt.Errorf("create list: %v", err)
    }
    defer res.Body.Close()

    resBody, err := io.ReadAll(res.Body)
    if err != nil {
        return fmt.Errorf("read resonse: %v", err)
    }

    if res.StatusCode >= 300 {
        return fmt.Errorf("retreive groups: %v (%s)", res.Status, resBody)
    }

    return nil
}

func (s *Session) AddTask(group, list, content string) (*doit.Task, error) {
    return s.AddTaskUnderDeadline(group, list, content, time.Time{})
}

func (s *Session) AddTaskUnderDeadline(groupName, listName, content string, until time.Time) (*doit.Task, error) {
    list, err := s.GetList(groupName, listName)
    if err != nil {
        return nil, fmt.Errorf("get ilst: %v", err)
    }

    reqBody := strings.NewReader(fmt.Sprintf(`{
  "list": %d,
  "name": "%s",
  "body": "",
  "is_done": false,
  "deadline": "%s"
}`, list.ID, content, until.Format(time.RFC3339)))

    req, err := s.newRequest("POST", "/todo/entries", reqBody)
    if err != nil {
        return nil, fmt.Errorf("form request: %v", err)
    }
    
    res, err := http.DefaultClient.Do(req)
    if err != nil {
        return nil, fmt.Errorf("add task: %v", err)
    }
    defer res.Body.Close()

    resBody, err := io.ReadAll(res.Body)
    if err != nil {
        return nil, fmt.Errorf("read resonse: %v", err)
    }

    if res.StatusCode >= 300 {
        return nil, fmt.Errorf("retreive groups: %v (%s)", res.Status, resBody)
    }

    var task *doit.Task
    err = json.Unmarshal(resBody, &task)
    if err != nil {
        return nil, fmt.Errorf("unmarshal task: %v", err)
    }

    return task, nil
}

func (s *Session) UpdateTask(*doit.Task) error { return nil }
func (s *Session) RemoveTask(group, list, id string) error { return nil }
