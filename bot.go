package doit

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/jonboulle/clockwork"
	"github.com/rs/zerolog/log"
	tele "gopkg.in/telebot.v3"
)

type Bot struct {
	SessionManager SessionManager
	Clock          clockwork.Clock

	sessions map[tele.ChatID]Session
}

func NewBot(manager SessionManager, clock clockwork.Clock) *Bot {
	return &Bot{
		SessionManager: manager,
		Clock:          clock,
		sessions:       make(map[tele.ChatID]Session),
	}
}

func (b *Bot) Login(userID tele.ChatID, username, pass string) error {
	session, err := b.SessionManager.NewUsingPassword(username, pass)
	if err != nil {
		return fmt.Errorf("login as %q: %v", username, err)
	}

	inboxExists, err := session.GroupExists("Inbox")
	if err != nil {
		return fmt.Errorf("check inbox group existence: %v", err)
	}

	if !inboxExists {
		err := session.CreateGroup("Inbox")
		if err != nil {
			return fmt.Errorf("create missing inbox group: %v", err)
		}
	}

	listExists, err := session.ListExists("Inbox", "Telegram")
	if err != nil {
		return fmt.Errorf("check telegram list existence: %v", err)
	}

	if !listExists {
		err := session.CreateList("Inbox", "Telegram")
		if err != nil {
			return fmt.Errorf("create missing telegram list: %v", err)
		}
	}

	b.sessions[userID] = session
	return nil
}

func (b *Bot) UpcomingTasks(chatID tele.ChatID) ([]*Task, error) {
	session, exists := b.sessions[chatID]
	if !exists {
		return nil, fmt.Errorf("no active session for @%v, login required", chatID)
	}

    tasks, err := b.getCanonicalTaskList(session)
	if err != nil {
		return nil, fmt.Errorf("retreive task list: %v", err)
	}

    now := b.Clock.Now()
    today := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local)

    for len(tasks) > 0 {
        if !tasks[0].Deadline.Before(today) {
            break
        }

        tasks = tasks[1:]
    }

    return tasks, nil
}

func (b *Bot) TomorrowTasks(chatID tele.ChatID) ([]*Task, error) {
	tasks, err := b.UpcomingTasks(chatID)
	if err != nil {
		return nil, fmt.Errorf("retreive upcoming tasks: %v", err)
	}

	now := b.Clock.Now()
	tomorrow := b.Clock.Now().AddDate(0, 0, 1)
	for i, task := range tasks {
		if task.Deadline.After(now) && task.Deadline.Before(tomorrow) {
			continue
		}

		tasks[i] = nil
	}

	return tasks, nil
}

// Returns a list of tasks grouped by the deadline and sorted by alphabet.
func (b *Bot) getCanonicalTaskList(s Session) ([]*Task, error) {
	list, err := s.GetList("Inbox", "Telegram")
	if err != nil {
		return nil, fmt.Errorf("retreive new task list: %v", err)
	}

	var unfinishedTasks []*Task
	for _, task := range list.Tasks {
		if task.Done {
			continue
		}

		unfinishedTasks = append(unfinishedTasks, task)
	}

	sort.Slice(unfinishedTasks, func(i, j int) bool {
		return unfinishedTasks[i].Content < unfinishedTasks[j].Content
	})

	sort.Slice(unfinishedTasks, func(i, j int) bool {
		return unfinishedTasks[i].Deadline.Before(unfinishedTasks[j].Deadline)
	})

	return unfinishedTasks, nil
}

// Will return a list of tasks that have unexpired deadlines...
func (b *Bot) HandleNewTask(msg *tele.Message) ([]*Task, error) {
	session, exists := b.sessions[tele.ChatID(msg.Sender.ID)]
	if !exists {
		return nil, fmt.Errorf("no active session for @%v, login required", msg.Sender.Username)
	}

	date, content, found := strings.Cut(msg.Payload, " ")
	if !found {
		return nil, fmt.Errorf("parse task deadline: not found (should be specified first)")
	}

	deadline, err := time.Parse("2006/01/02", date)
	if err != nil {
		return nil, fmt.Errorf("parse task deadline: %v", err)
	}

	session.AddTaskUnderDeadline("Inbox", "Telegram", content, deadline)

	tasks, err := b.getCanonicalTaskList(session)
	if err != nil {
		return nil, fmt.Errorf("retreive new task list: %v", err)
	}

	return tasks, nil
}

func (b *Bot) Poll(at time.Time, cb func(chatID tele.ChatID, tasks []*Task, err error)) {
	now := b.Clock.Now()

    nextPollTime := time.Date(now.Year(), now.Month(), now.Day(), at.Hour(), at.Minute(), at.Second(), at.Nanosecond(), at.Location())
    if now.After(nextPollTime) {
        nextPollTime = nextPollTime.AddDate(0, 0, 1)
    }

    for {
        log.Debug().Str("sleeping_for", nextPollTime.Sub(b.Clock.Now()).String()).Msg("Sleeping")
        b.Clock.Sleep(nextPollTime.Sub(b.Clock.Now()))
        nextPollTime = nextPollTime.AddDate(0, 0, 1)

        log.Debug().Msg("Polled!")

        for chatID := range b.sessions {
            tasks, err := b.TomorrowTasks(chatID)
            if err != nil {
                cb(chatID, nil, err)
                continue
            }

            cb(chatID, tasks, nil)
        }
    }
}

